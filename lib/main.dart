import 'package:flutter/material.dart';

class Counter extends StatefulWidget {
  @override
  _CountState createState() => _CountState();
  
}

class _CountState extends State<StatefulWidget> {
  int _counter = 0;
  void _increment() {
    setState(() {
      _counter++;
    });
    
  }
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ElevatedButton(onPressed: _increment, child: Text('Icrement')),
        SizedBox(width: 16,
        ),
        Text('Counter: $_counter'),
      ],
    );
  }
}

void main() {
  runApp(MaterialApp(
    home: Scaffold(
        body: Center(
      child: Counter(),
    )),
  ));
}
